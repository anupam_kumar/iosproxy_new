//
//  ViewController.m
//  iosproxy
//
//  Created by Kumar Anupam on 10/14/16.
//  Copyright © 2016 Kumar Anupam. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)connectButtonPressed:(id)sender {
    NSLog(@"Connect Button pressed");
    NSString *statusLabelText = [NSString stringWithFormat:@"Connecting..."];
    self.status_label.text = statusLabelText;
    //[self initNetworkCommunication];
}

- (IBAction)loadProcessesButtonPressed:(id)sender {
    NSLog(@"Load Processes Button pressed");
    NSString *statusLabelText = [NSString stringWithFormat:@"Calculator\nNotepad\nExplorer"];
    self.status_label.text = statusLabelText;
}

- (IBAction)disconnectButtonPressed:(id)sender {
    NSLog(@"Disconnect Button pressed");
    NSString *statusLabelText = [NSString stringWithFormat:@"Disconnecting..."];
    self.status_label.text = statusLabelText;
    //destroyNetworkCommunication();
    statusLabelText = [NSString stringWithFormat:@"Disconnected"];
    self.status_label.text = statusLabelText;
    NSLog(@"Disconnected from server");
}
@end
