//
//  main.m
//  iosproxy
//
//  Created by Kumar Anupam on 10/12/16.
//  Copyright © 2016 Kumar Anupam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
