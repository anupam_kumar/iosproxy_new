//
//  AppDelegate.h
//  iosproxy
//
//  Created by Kumar Anupam on 10/12/16.
//  Copyright © 2016 Kumar Anupam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

