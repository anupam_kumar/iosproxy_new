//
//  ViewController.h
//  iosproxy
//
//  Created by Kumar Anupam on 10/14/16.
//  Copyright © 2016 Kumar Anupam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
- (IBAction)connectButtonPressed:(id)sender;
- (IBAction)loadProcessesButtonPressed:(id)sender;
- (IBAction)disconnectButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *status_label;

@end
